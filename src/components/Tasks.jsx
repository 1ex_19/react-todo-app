import Task from "./Task";

export default function Tasks({ tasks, remove }) {

    // Возращаем на каждую переданную запись по компоненту Task
    // в которые передаем взятые сверху (с родительского компонента) метод remove,
    // объект, передающийся через map - item({ text: 'some task text', id: 1 })
    // и атрибут key, который необходим реакту, что бы отличать один однородный компонент (имеется в виду, что они все Task) от другого.
    // key должен быть уникальным для каждого елемента возвращаемого одной map функцией, а лучше вообще уникальным средм всего.
    const createTasks = (item) => {
        return <Task remove={remove} key={item.id} task={item} />;
    };

    return (
        <div className="row tasks justify-content-center">
            <div className="col-12 col-lg-4 col-md-4">
                {/* 
                    Используя функцию map возвращаем на место массива tasks массив результатов функции createTasks,
                    которая в свою очередь возвращает компоненты Task.
                    Реакт увеет автоматически распаковывать массивы и отображать их
                 */}
                {tasks.map(createTasks)}
            </div>
        </div>
    );
}
