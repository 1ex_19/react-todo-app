import { useRef, useState } from "react";

export default function Input({ add }) {
    // Тут будем хранить текст задания до момента нажатия на кнопку "Create"
    const [task, set] = useState(null);
    // Создав таким образом переменную и потом добавив её через атрибут ref к блоку, мы можем потом ссылаться на нее, как если бы вызывали document.getElemntById()
    const _input = useRef();

    // Функция для записи текста задания во временную переменную task где она будет храниться до момента нажатия на кнопку "Create"
    const updateTask = (event) => {
        // Через event.target мы получаем объект обычного браузерного события. Target необходим,
        // т.к. по event сам по себе не есть объект события, а реактовская обертка над ним
        let value = event.target.value;

        // Таким синтаксисом мы избегаем использования if else. Только если значение слева будет правдой - выполнится выражение справа.
        value !== null && set(value);
    };

    // Функция, которую мы привязали через onSubmit на событие "submit" формы.
    const submit = (event) => {
        // Несмотря на то, что, как уже выше говорилось, event передаваемый в функции добавленные через onЧтоугодно атрибуты блоков (onClick, onSubmit, onChange, ...)
        // не является стандартным eventom который мы имеем возможность получить внутри стандартных методов js типа addEventListener('click', function(event){})
        // в нем предусмотрена такой метод, как preventDefault, который ведет себя так, как и ожидается от обычного preventDefault.
        event.preventDefault();
        if (task !== null && task !== "") {
            // Тут мы очищаем поле инпута
            _input.current.value = "";

            // отправляем значение нашей временной переменной task (оно же значение инпута) наверх (в родительский компонент) через функцию add,
            // которую мы передали через свойства компонента (в документации их называют пропсы) и приняли тут через распаковку js объектов
            add(task);

            // очищаем нашу временную переменную.
            // Кстати мы могли ее не использовать и напрямую брать значение для передачи наверх из инпута обычными js методами.
            set(null);

            // тут ставим таймаут с разфокусиванием кнопки create, т.к. самостоятельно она этого не сделает
            setTimeout(() => {
                event.target.submitButton.blur();
            }, 200);
        }
    };

    // Тут вполне себе непримечательная верстка.
    // обычные свойства в аттрибуты типа id или name прописываются как обычно,
    // некоторые атрибуты (таки как for, class, ...) изменяются, т.к. эти слова забронированы в js
    // под другие нужды (полагаю объяснять что такое for или class в js не нужно)
    
    return (
        <div className="row justify-content-center">
            <div className="col-12 col-lg-4 col-md-4">
                <form onSubmit={submit} className="py-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">
                        Task text
                    </label>
                    <div className="input-group">
                        <input
                            ref={_input}
                            onInput={updateTask}
                            placeholder="Set task text"
                            type="text"
                            name="text"
                            className="form-control"
                        />
                        <button
                            type="submith"
                            name="submitButton"
                            className="btn btn-outline-primary"
                        >
                            Create
                        </button>
                    </div>
                    <div id="textHelp" className="form-text">
                        Tasks without text aren't allowed
                    </div>
                </form>
            </div>
        </div>
    );
}
