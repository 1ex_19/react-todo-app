import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'


// Для того, что реакт вообще работал, неоюходимо корневой компонент (в котором все будет работать)
// передать в метод render объекта ReactDOM и указать в какой html элемент на странице ему рендериться.
ReactDOM.render(
  // Тут корневой компонент App обернут в другой, стандартный реактовский компонент, но этого можно не делать.
  // Он просто делает всякие проверки перед непосредственно рендером или как-то так, я хз.
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)
