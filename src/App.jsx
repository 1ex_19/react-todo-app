import { useEffect, useState } from "react";
import Input from "./components/Input";
import Tasks from "./components/Tasks";
import "./App.css";

// Тут будет храниться инкрементирующееся число, которое мы будем использовать для генерации id.
// Объявляем его ВНЕ компонента, т.к. если объявить внутри - при каждой перерисовке компонента (например добавится новый таск)
// все переменные (которые не созданы с помощью useState ) создаются заново.
// А тут она создастся только при первом запуске страницы.
let increment = 0;

export default function App() {

    // Тут храним ключ, по которому будем сохранять и получать данные из хранилища в виде сериализированной строки.
    const storageKey = "todoapp_tasks";

    const [tasks, setTasks] = useState([]);
    
    // Для того, что бы хранить таски между запросами
    // мы используем сессионное хранилище браузера.
    // Данные будут сохранены до закрытия браузера.
    const storage = window.sessionStorage;

    // useEffect - это функция, которая выполняется при изменении каких то переменных созданных с использованием функции useState или переданных в компонент 
    // через аттрибуты компонента (пропсы).
    // Что бы слушать какую либо переменную ее необходимо указать в массивных скобочках после объявления функции слушателя, 
    // там, где у данного useEffect просто пустые скобки [].
    // Пустые скобки [] - обозначают, что эта функция выполнится только один раз, при первой отрисовке компонента и больше никогда.
    useEffect(() => {
        let lasttasks = getTasksFromStorage();
        increment =
            lasttasks.length > 0 ? lasttasks[lasttasks.length - 1].id : 0;
        setTasks(lasttasks);
    }, []);

    // Этот useEffect выполнится только, если изменится переменная tasks.
    useEffect(() => {
        updateTasksOnStorage();
    }, [tasks]);


    // Отдельная функция для первоначального получения данных из хранилища (пердполагаем, что мы уже могли добавлять таски и у нас есть какие-то сохраненные данные)
    // Она вернет либо данные, либо, если их нет (это первый запуск страницы и мы ни одного задания не добавили еще) просто пустой массив.
    const getTasksFromStorage = () => {
        let t = storage.getItem(storageKey);
        if (t === null) return [];

        try {
            return JSON.parse(t);
        } catch (error) {
            return [];
        }
    };


    // Функция удаляющая определенные таск из общего массива, по его id.
    const removeTask = (task) => {
        // Просто перезаписывается весь массив на другой, в котором уже нет заданного таска
        setTasks(tasks.filter((item) => item.id !== task.id));
    };

    // Добавляем новый таск в массив используя полученый от компонента Input текст и генерируя для таска новый id,
    // уникальный для текущей сессии.
    const addTask = (task) => {
        setTasks([
            ...tasks,
            {
                text: task,
                id: ++increment,
            },
        ]);
    };
    
    
    // При каждом изменении массива с тасками (удалении или добавлении нового таска)
    // обновляем наше хранилище. 
    const updateTasksOnStorage = () => {
        storage.setItem(storageKey, JSON.stringify(tasks));
    };

    return (
        <div className="m-5">
            <div className="container">
                <Input add={addTask} />
                <Tasks remove={removeTask} tasks={tasks} />
            </div>
        </div>
    );
}
